import logging
import multiprocessing
import time


def setup_logger(logger_name, log_file, level=logging.INFO):
    l = logging.getLogger(logger_name)
    formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(name)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    file_handler = logging.FileHandler(log_file, mode='w')
    file_handler.setFormatter(formatter)
    l.addHandler(file_handler)
    l.setLevel(level)


def f(x):
    # Get logical process ID (i.e. starting from 1, not the PID which changes every time!)
    logical_pid = multiprocessing.current_process()._identity[0]

    # Get logger for current process ID and initialize if not currently initialized
    logger_name = "process_{:d}".format(logical_pid)
    log = logging.getLogger(logger_name)
    if not log.handlers:
        log_file = "{}.log".format(logger_name)
        setup_logger(logger_name, log_file, logging.DEBUG)

    # Calculate result and log it to file
    result = x*x
    log.debug("x = {:<3d} --> x*x = {:d}".format(x, result))

    # Since function completes very quickly, add small wait before returning to ensure function
    # is more evenly distributed across processes (for testing purposes only, remove in production code!)
    time.sleep(0.1)
    return result


def main():
    # Initialize main logger (this is the only one that will output to stdout (or stderr))
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(levelname)-8s %(name)s: %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S',
                        handlers=[logging.StreamHandler()])

    # Create pool of worker processes
    num_processes = 4
    log = logging.getLogger(__name__)
    log.info("Creating pool of {:d} worker processes".format(num_processes))
    pool = multiprocessing.Pool(num_processes)

    # Calculate function in parallel
    log.info("Starting parallel processing...")
    for i in range(101):
        result = pool.apply_async(f, [i])

    # Since f() is called asynchronously, we have to block here to stop program terminating prematurely
    pool.close()
    pool.join()
    log.info("Finished processing; f(100) = {:,d}".format(result.get()))

if __name__ == '__main__':
    main()
